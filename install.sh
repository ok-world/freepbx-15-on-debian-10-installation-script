 #!/bin/bash

apt update && apt dist-upgrade -y

apt-get install git curl wget \
libnewt-dev libssl-dev libncurses5-dev \
subversion libsqlite3-dev build-essential \
libjansson-dev libxml2-dev  uuid-dev \
lm-sensors libsox-fmt-all sox pkg-config \
asterisk asterisk-ooh323 speex asterisk-mp3 -y

apt -y install mariadb-server mariadb-client

apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates
curl -sL https://deb.nodesource.com/setup_12.x | bash -

apt update && apt -y install gcc g++ make nodejs

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update && apt-get install yarn -y

apt -y install apache2

cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf_orig
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/apache2/apache2.conf
sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

rm -f /var/www/html/index.html

apt -y install wget php php-pear php-cgi php-common php-curl php-mbstring php-gd php-mysql php-gettext php-bcmath php-zip php-xml php-imap php-json php-snmp php-fpm libapache2-mod-php

sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php/7.3/apache2/php.ini
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php/7.3/cli/php.ini

apt -y install wget
wget http://mirror.freepbx.org/modules/packages/freepbx/freepbx-15.0-latest.tgz

tar xfz freepbx-15.0-latest.tgz
rm -f freepbx-15.0-latest.tgz

cd freepbx
./start_asterisk start
./install -n

a2enmod rewrite
systemctl restart apache2

fwconsole ma downloadinstall framework --force

fallocate /swapfile -l 4G
mkswap /swapfile
chmod 0600 /swapfile
swapon /swapfile
